// console.log("Hello World")

// [SECTION] Functions
	// Functions in javascript are line/blocks of codes that  tell our device/application to perform a specific task when called/invoke.
	// it prevents repeating lines/blocks of codes that perform the same task/function.
	
	// Function Declarations

/*
	Syntax:
		function functionName(){
		code block (statement)
		}
	- function keyword - used to define a javascript function.
	- functionName - name of the function, which will be used to call/invoked the function
	- function block({}) - indicates the function body.
*/

function printName(){
	console.log("My name is John");
}

// Function Invocation
	// This run/execute the code block inside the function.
	printName("My name is John");

// Function Invocation
 // The code block and statements inside a function is not immediately executed when function is defined.
 // THis run/execute the code block inside the fucntion when "called/invoked".

	printName();

	declaredFunction(); // we cannot invoke a function that we have not declared/defined yet.

// [SECTION] Function Declarations vs Function Expressions

	// FUnction Declaration
		// function can be created by using function keyword and adding a function name.
		//  "saved for later use"	
		// Declared functions can be "hoisted", as long as a function has been defined.
			// Hoisting is JS behavior for certain variables and funcitons to run or use them before their declaration.


	function declaredFunction(){
		console.log("Hello World from declaredFunction()");
	}

	// Function Expression
		// A funciton can also be stored in a variable.

		/*
			Syntax:
				let/const variableName= function(){
					//code block (statement)
				}

				-function(){}-  Anonymous function, a function without a name.
		*/

		//variableFunction(); // error - function expression, being stored in a let/const variable, 

		let variableFunction = function(){
			console.log("Hello Again!");
		}

		variableFunction();

		// We create also function expression with a named function.

		let funcExpression = function funcName(){
			console.log("Hello from the other side.")
		}

		// funcName(); //funcName() is not defined
		funcExpression(); // to invoke the function, we invoke it by its variable name and not by its function name

		// Reassign declared function and function expression to a new anonymous function.

		declaredFunction = function(){
			console.log("updated declaredFunction")
		}

		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression")
		}

		funcExpression();

		// we cannot reassign a function expression initialized with const

		const constantFunc = function(){
			console.log("Initialized with const")
		}

		constantFunc();

		// This will result to reassignment error
		// constantFunc = function(){
		// 	console.log("Cannot be reassigned");
		// }

		// constantFunc();

// [SECTION] Function Scoping
		/*
			Scope is the accessibility (visibility) of variables.

			JavaScript Variable has 3 types of scope:
			1. global scope
			2. local/block scope
			3. function scope
		*/

		// Global Scope
				// variable can be accessed anywhere from the program.

		let globalVar = "Mr. Worldwide";

		// Local Scope
				// variables declared inside a curly bracket ({}) cann only be accessed locally.

		// console.log(localVar);

		{
			// var localVar = "Armando Perez";
			let localVar = "Armando Perez";
		}

		console.log(globalVar);
		// console.log(localVar); //result in error cannot be accessed outside its code block

		// Function Scope

		function showNames(){}
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Joey";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);


		showNames();

		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);

		function myNewFunction(){
			let name = "Jane";

			function nestedFunc(){
				let nestedName = "John"
				console.log(name);
			}
			// console.log(nestedName);
			nestedFunc();
		}

		myNewFunction();

		// nestedFunc(); //result to an error

		// Global Scope Variable

		let globalName = "Alex";

		function myNewFunction2(){
			let nameInside = "Renz";
			console.log(globalName);
			console.log(nameInside);
		}

		myNewFunction2();

		// console.log(nameInside); //only accessible on the function scope.

// [SECTION] Using alert() and prompt()
	 //alert() allows us to show small window at the top of our browser page to show information to our users.

	 // alert("Hello World"); // this will run immediately when the page reloads.

	 // You can use an alert() to show a message to the user from a later function invocation. 

	 function showSampleAlert(){
	 	alert("Hello, User");
	 }

	 showSampleAlert();

	 console.log("I will only log in the consle when the alert is dismissed.");

	 // notes on the use of alert():

	 	// show only an alert() for short dialogs/messages to the user.
	 	// do not overuse alert() because the program has to wait for it to be dismissed before it continues.

	 // prompt() allows us to show a small window at the top of the browser to gather user input.
	 // the input from the prompt() will return as a "String" once the user dismissed the window.

	 /*
		Syntax: 
		let/const variableName = prompt("<diaglogInString>");
	 */

	 // let name = prompt("Enter your name: ");
	 // let age = prompt("Enter your age: ");

	 // console.log(typeof age);

	 // console.log("Hello I am, "+name+", and I am "+age+" years old.");

	 // let sampleNullPrompt = prompt("Do  Not Enter Anything");

	 // console.log(sampleNullPrompt);

	 //prompt() return an "empty string" ("") when there is no user input and we have clicked okay or "null" if the user cancels the prompt.

	 function printWelcomeMessage(){
	 	let name = prompt("Enter your name: ");

	 	console.log("Hello, "+ name + "! Welcome to my page!");
	 }

	 printWelcomeMessage();

// [SECTION] Function Naming Conventions

	 // 1. Function names should be definitive of the task it will perform.  It usually contains a verb.
	 	// functions also follows camelCase for naming it.

	 	function getCourses(){
	 		let courses = ["Science 101", "Math 101", "English 101"];
	 		console.log(courses);
	 	}

	 	getCourses();

	 // 2. Avoid generic names to avoid confusion within your code.

	 	function get(){
	 		let name = "Jamie";
	 		console.log(name);
	 	}

	 	get();

	 // 3. Avoid pointless and inappropriate function names, example: foo, bar (metasyntactic variable - placeholder variables).

	 	function foo(){
	 		console.log(25%5)
	 	}